
class AccessDeniedError < StandardError
end
class NotAuthenticatedError < StandardError
end
class AuthenticationTimeoutError < StandardError
end

class AuthenticatedController < ApplicationController

  # When an error occurs, respond with the proper private method below
  rescue_from AuthenticationTimeoutError, with: :authentication_timeout
  rescue_from NotAuthenticatedError, with: :user_not_authenticated

  def index
    head 200
  end

  protected

  def auth_user
    return @user if @user
    json = UsersService.get("users/" + user_id).body
    @user ||= User.new JSON.parse(json)
  rescue JWT::ExpiredSignature
   raise AuthenticationTimeoutError
  rescue JWT::VerificationError, JWT::DecodeError
   raise NotAuthenticatedError
  end

  def auth_user_id
    auth_user.id
  end


end
