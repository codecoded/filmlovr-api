class FilmActionsController < AuthenticatedController

  PAGE_LIMIT = 50

  def create
    film_action = FilmAction.new user: auth_user_id, film: film_id, film_action: user_action
    logger.debug "Creating film action: #{film_action.to_json}"
    response = GraphAPI.action_film film_action.to_json
    render json: response.body, status: response.status
  end

  def delete
    film_action = FilmAction.new user: auth_user_id, film: film_id, film_action: user_action
    logger.debug "Deleting film action: #{film_action.to_json}"
    response = GraphAPI.delete_film_action film_action
    render json: response.body, status: response.status
  end

  def film_id
    params[:id]
  end


  def user_action
    params[:user_action].downcase
  end

end
