class FilmsController < ApplicationController

  PAGE_LIMIT = 50

  def index
    render json: FilmsAPI.films(pageOptions)
  end

  def show
    render json: FilmsAPI.film(film_id, user_id)
  end

  def upcoming
    render json: FilmsAPI.upcoming(user_id, pageOptions)
  end

  def in_cinema
    render json: FilmsAPI.now_playing(user_id, pageOptions)
  end

  def film_id
    params[:id]
  end

  def pageOptions
    {
      limit: params[:limit] || PAGE_LIMIT,
      skip: params[:skip] || 0,
      sort: params[:sort] || 'popularity'
    }
  end

end
