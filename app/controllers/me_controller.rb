class MeController < AuthenticatedController

  def show
    result = UsersService.get("users/" + user_id)
    render json: result.body, status: result.status
  end


end
