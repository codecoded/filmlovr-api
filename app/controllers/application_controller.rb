class ApplicationController < ActionController::API

  def index
    head 200
  end


  protected

  def user_id
    decoded_auth_token[:user_id] if decoded_auth_token
  end

  def decoded_auth_token
    @decoded_auth_token ||= AuthToken.decode(http_auth_token) if !http_auth_token.blank?
  end

  def http_auth_token
   @http_auth_token ||= auth_header.split(' ').last
  end

  def auth_header
    request.headers["Authorization"] || ''
  end


  def authentication_timeout
    render json: { errors: ['Authentication Timeout'] }, status: 419
  end
  def forbidden_resource
    render json: { errors: ['Not Authorized To Access Resource'] }, status: :forbidden
  end
  def user_not_authenticated
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

end
