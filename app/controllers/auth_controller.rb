class AuthController < ApplicationController

  def create
    send auth_hash.provider  if respond_to? auth_hash.provider
  end

  def facebook_me
    passport = Passport.new passport_params
    render json: FacebookService.me(passport.token)
  end

  def login
    passport = Passport.new passport_params
    result = PassportsAPI.login passport

    if result.status != 200
      if passport.protocol == 'oauth2'
        passport.userdata = ouath_user_data(passport)
        result = PassportsAPI.register passport
      end
    end

    try_authorise result
  end

  def ouath_user_data(passport)
    if passport.provider == "facebook"
      FacebookAPI.new(passport.token).user_data
    else
      raise "OAuth provider #{passport.provider} not implemented"
    end
  end

  def signup
    user = User.new userdata
    passport = Passport.new protocol: 'password',
      provider: 'local',
      identifier: user.email,
      token: password,
      userdata: user
    try_authorise PassportsAPI.register passport
  end

  def facebook
    logger.debug auth_hash
    user = User.new username: username, email:email
    passport = Passport.new   protocol: 'oauth2',
      provider: 'facebook',
      identifier: auth_hash.uid,
      token: creds.token,
      expires: creds.expires_at,
      userdata: user,
      user: username

    result = PassportsAPI.login passport
    result = PassportsAPI.register passport if result.status != 200
    try_authorise result
    #
  end

  def try_authorise(result)

    if result.status != 200
      # Wrap in error handler
      render json: result.body, status: result.status
      return
    end
    user = User.new JSON.parse(result.body)
    json =  {
              username: user.username,
              token: JWT.encode(user.tokenize, Rails.application.secrets.secret_key_base, 'HS256')
            }
    render json: json, status: result.status
  end

  protected

  def captureError
  end

  def userdata
    params[:userdata].permit(:username, :email)
  end

  def password
    params[:token]
  end

  def username
    name =  user_info ? user_info.name : userdata[:username]
    name.parameterize.underscore if name
  end

  def email
     user_info ? user_info.email : userdata[:email]
  end

  def passport_params
    params.permit(:provider, :identifier, :user, :token, :protocol, :userdata)
  end

  def user_info
    auth_hash['info'] if auth_hash
  end

  def creds
    auth_hash['credentials']
  end

  def auth_hash
    request.env['omniauth.auth']
  end
end
