class FilmsAPI
  class << self

    def films(options={})
      FilmlovrService.get :films, options
    end

    def upcoming(user_id = nil, options = {})
      FilmlovrService.get "films/upcoming", options
    end

    def now_playing(user_id = nil, options={})
      FilmlovrService.get "films/now_playing", options
    end

    def film(id, user_id = nil)
      FilmlovrService.get "films/#{id}", {userId: user_id}
    end



  end
end
