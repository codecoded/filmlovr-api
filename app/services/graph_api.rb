class GraphAPI
  class << self

    def action_film(film_action, options={})
      GraphService.post :edges, film_action.to_json
    end

    def delete_film_action(film_action, options={})
      GraphService.delete "edges/#{film_action.id}"
    end
  end
end
