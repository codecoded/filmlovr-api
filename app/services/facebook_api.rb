class FacebookAPI

  # attr_reader :token, :graph
  def initialize(oauth_access_token)
    @token = oauth_access_token
    @graph = Koala::Facebook::API.new(oauth_access_token)
  end


  def self.me(oauth_access_token)
    new(oauth_access_token).me
  end

  def me
    @graph.get_object("me")
  end

  def user_data
    data = me
    User.new username: snake_case(data['name']), email: data['email']
  end

  def snake_case(val)
    val.gsub(' ','_').parameterize.underscore if val
  end
end
