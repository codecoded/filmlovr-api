class GraphService
  class << self

    def uri
      "http://database:8529/_db/_system/graph"
    end

    def post(method, options=nil)
      Rails.logger.debug "#{uri}/#{method}"
      process(Faraday.post"#{uri}/#{method}", options)
    end

    def delete(method, options=nil)
      Rails.logger.debug "DELETE #{uri}/#{method}"
      process(Faraday.delete "#{uri}/#{method}", options)
    end

    def process(response)
      response
    end
  end
end
