class PassportsAPI
  class << self

    def register(passport)
      UsersService.post :passports, passport.to_json
    end

    def login(passport)
      UsersService.put "passports/#{passport.identifier}", passport.to_json
    end

  end
end
