class UsersService
  class << self

    def uri
      "http://database:8529/_db/_system/users"
    end

    def get(method, options=nil)
      Rails.logger.debug "GET #{uri}/#{method}"
      process(Faraday.get"#{uri}/#{method}", options)
    end

    def post(method, body, options=nil)
      Rails.logger.debug "POST #{uri}/#{method}, BODY=#{body}"
      process(Faraday.post "#{uri}/#{method}", body)
    end

    def put(method, body={}, options=nil)
      Rails.logger.debug "PUT #{uri}/#{method}, BODY=#{body}"
      process(Faraday.put "#{uri}/#{method}", body)
    end

    def process(response)
      if response.status != 200
        Rails.logger.error response.body
        Rails.logger.error response.status
      end
      response
    end
  end
end
