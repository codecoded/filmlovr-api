class FilmlovrService
  class << self

    def uri
      "http://database:8529/_db/_system/filmlovr"
    end

    def get(method, options=nil)
      Rails.logger.debug "#{uri}/#{method}"
      process(Faraday.get"#{uri}/#{method}", options)
    end

    def process(response)
      response.body
    end
  end
end
