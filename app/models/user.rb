class User
  include ActiveModel::Model
  attr_accessor  :id, :_key, :username, :email, :token

  def id
    _key
  end

  def tokenize
    {user_id: _key}
  end


end
