class FilmAction
  include ActiveModel::Model
  attr_accessor :user, :film, :film_action

  def id
    "#{user}_#{film_action}_#{film}"
  end

  def _from
    "users/#{user}"
  end

  def _to
    "films/#{film}"
  end

  def to_json
    {
      "_key": id,
      "_from": _from,
      "_to": _to,
      "type": film_action.downcase
    }
  end
end
