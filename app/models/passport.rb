class Passport
  include ActiveModel::Model
  attr_accessor :provider, :identifier, :protocol, :user, :token, :expires, :userdata
end
