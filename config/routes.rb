Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/films/upcoming', to: "films#upcoming"
  get '/films/in_cinema', to: "films#in_cinema"

  resources :films, only: [:index, :show]

  get '/me', to: "me#show"

  post  '/films/:id/:user_action', to: "film_actions#create"
  delete '/films/:id/:user_action', to: "film_actions#delete"

  # get '/register/facebook', to:  "register#facebook"
  post '/auth/login/', to: "auth#login"
  post '/auth/signup/', to: "auth#signup"
  post '/auth/facebook/me/', to: "auth#facebook_me"

  get '/auth/:provider/callback', to: "auth#create"

  # root "application#index"
  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
